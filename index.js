const express = require('express')
const app = express()
const port = 5000

app.use(express.static(__dirname + '/assets'))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/')
})

app.get('/pages/experiences', (req, res) => {
    res.sendFile(__dirname + '/pages/experiences.html')
})

app.get('/pages/contact', (req, res) => {
    res.sendFile(__dirname + '/pages/contact.html')
})

app.listen(port, () => console.log("oke"))